package com.assignment4a;

import java.io.IOException;

import static com.assignment4a.util.Constant.*;

public class Main {
    public static void main(String args[]) throws IOException {
        UploadFiletoHDFS obj=new UploadFiletoHDFS();
        obj.storeSerializedFile(EmployeeSerializedFilePath,EMPLOYEE_HDFS_OUTPUT_PATH);
        obj.storeSerializedFile(BuildingSerializedFilePath,BUILDING_HDFS_OUTPUT_PATH);

        HbaseTableCreator hbaseTableCreator=new HbaseTableCreator();
        hbaseTableCreator.createTableBuilding(BUILDING_TABLE_NAME);
        hbaseTableCreator.createTableEmployee(EMPLOYEE_TABLE_NAME);

    }
}
