package com.assignment4a;

import static com.assignment4a.util.Constant.*;

import ProtoFiles.Building.Building;
import ProtoFiles.Building.BuildingList;
import ProtoFiles.Employee.Employee;
import ProtoFiles.Employee.EmployeeList;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;

import java.util.*;

class EmployeeMapper extends Mapper<NullWritable, BytesWritable, ImmutableBytesWritable, Put> {

    ImmutableBytesWritable TABLE_NAME_TO_INSERT = new ImmutableBytesWritable(Bytes.toBytes(EMPLOYEE_TABLE_NAME));

    public void map(NullWritable key, BytesWritable value, Context context) {
        try {

            byte b[] = value.getBytes();
            EmployeeList employeeList = EmployeeList.parseFrom(Arrays.copyOf(value.getBytes(), value.getLength()));
            for (Employee employee : employeeList.getEmployeesList()) {
                int employee_id = employee.getEmployeeId();
                //byte byteArray[] = Bytes.toBytes(employee_id);

                Put put = new Put(Bytes.toBytes(String.valueOf(employee_id)));
                put.addColumn(Bytes.toBytes(EMPLOYEE), Bytes.toBytes("employee_id"),
                        Bytes.toBytes(String.valueOf(employee_id)));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("name"),
                        Bytes.toBytes(employee.getName()));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("building_code"),
                        Bytes.toBytes(employee.getBuildingCode()));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("floor_number"),
                        Bytes.toBytes(String.valueOf(employee.getFloorNumberValue())));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("salary"),
                        Bytes.toBytes(String.valueOf(employee.getSalary())));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_EMPLOYEE), Bytes.toBytes("department"),
                        Bytes.toBytes(employee.getDepartment()));

                //Employee_Id,Name,Building_Code,Floor_Number,Salary,Department
                context.write(TABLE_NAME_TO_INSERT, put);
            }


        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}

class BuildingMapper extends Mapper<NullWritable, BytesWritable, ImmutableBytesWritable, Put> {

    ImmutableBytesWritable TABLE_NAME_TO_INSERT = new ImmutableBytesWritable(Bytes.toBytes(BUILDING_TABLE_NAME));

    public void map(NullWritable key, BytesWritable value, Context context) {
        try {

            byte b[] = value.getBytes();
            BuildingList buildingList = BuildingList.parseFrom(Arrays.copyOf(value.getBytes(), value.getLength()));
           // int i = 1;
            for (Building building : buildingList.getBuildingList()) {

                Put put = new Put(Bytes.toBytes(building.getBuildingCode()));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_BUILDING), Bytes.toBytes("building_code"),
                        Bytes.toBytes(building.getBuildingCode()));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_BUILDING), Bytes.toBytes("floors"),
                        Bytes.toBytes(String.valueOf(building.getTotalFloors())));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_BUILDING), Bytes.toBytes("companies"),
                        Bytes.toBytes(String.valueOf(building.getTotalCompanies())));
                put.addColumn(Bytes.toBytes(COLUMN_FAMILY_BUILDING), Bytes.toBytes("cafeteria_code"),
                        Bytes.toBytes(building.getCafeteriaCode()));

                context.write(TABLE_NAME_TO_INSERT, put);
               // i++;

            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}