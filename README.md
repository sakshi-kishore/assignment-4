<b>proto objects</b> - src/main/proto

Building.proto (Proto object for building table)
Employee.proto (Proto object for employee table)

<b>csv data</b> - src/resources/

csv files to populate Building and Employee table

<h1><b><u>Assignment 4a</u></b></h1>

<b>CSVtoProto.java</b>

Creates serialized proto file from above mentioned csv
Output path - resources/ProtoOutputFiles

creating tables

<b>UploadFiletoHDFS.java</b>

To store serialized proto file from local to HDFS

<b>HbaseTableCreator.java</b>

To create Building and Employee table
Uses Admin.createTable method

<b>Main.java</b>

Calls UploadFiletoHDFS and HbaseTableCreator methods internally
Contains main method

insert data into hbase tables

<b>MRDriver.java</b>

Driver class for mapreduce job for creating HFiles
Calls driver method for Employee and Building each
Calls bulkLoad method after job completion

<b>HbaseMapper.java</b>

Writes data into Put objects with column qualifiers:

1. employee - Employee_Id,Name,Building_Code,Floor_Number,Salary,Department
2. building - building_code, floors, companies, cafeteria_code

<h1><b><u>Assignment 4b</u></b></h1>

<b>CafeteriaDriver.java</b>

Scans tables employee and building and apply joins based on building_code

Mapper assigns building code to each Employee row - Iterator(Result)

Reducer inserts cafe code through Put object into employee table

Mapper class - CafeteriaMapper.java

Reducer class - CafeteriaReducer.java